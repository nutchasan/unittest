import unittest
import fizzbuzz


class TestFizzBuzz(unittest.TestCase):
    def test_give_3_should_fizz(self):
        num = 3
        expected = 'fizz'
        result = fizzbuzz.fizzbuzz(num)
        self.assertEqual(expected, result)


if __name__ == '__main__':
    unittest.main()
